/*
 *   -----------------------------------------------------
 *   helper.js
 *   Script for any helper for HRD
 *   -----------------------------------------------------
 *
 *   Juni 2019
 *
 *   rahmadalhabib89@gmail.com
 *
 *
 */
let searchID;
var displayed_dept = new Array();
var data_ovt = [];

/* printing */
const registerButtonPrint = () => {
    $("#print").click(function () {
        window.print();
    });
}

/* excel */
const registerButtonExcel = () => {
    $("#excel").click(function () {
        TableExport(document.getElementsByTagName("table"), {
            formats: ["xlsx"],
            position: "top",
            bootstrap: true,
            filename: 'hrd-' + moment().format("d-M-YY-H-mm-ss")
        });
    });
}

const getRole = () => {
    url = "/admin/getrole";

    $.ajax({
        type: "GET",
        url: url,
        beforeSend: function () {},
        complete: function () {},
        success: function (data) {
            if (data.role != "HRD") {
                let value = $("body").attr("class");
                value = value.replace("skin-purple", "skin-blue");
                $("body").attr("class", value);
            }
        }
    });
}

const selectPeriode = () => {
    $("#periode").daterangepicker({
            opens: "left",
            minViewMode: "month",
            startDate: new Date(),
            endDate: new Date()
        },
        function (start, end, label) {
            if (start.format("YYYY") != end.format("YYYY")) {
                $("#periode").val("");
                alert("Periode harus dalam tahun yang sama");
            }
            startmonth = start.format("YYYY-MM-DD");
            endmonth = end.format("YYYY-MM-DD");
        }
    );
}

const registerButtonSearch = () => {
    $("#search").click(function () {
        switch (searchID) {
            case 1:
                if (!$("#periode").val()) {
                    alert("PERIODE harus diisi.");
                    return;
                }
                defaultStartEnd();
                var pt = $("#select_pt").val();
                var request =
                    "?startmonth=" + startmonth + "&endmonth=" + endmonth + "&pt=" + pt;
                resetTable();
                getReportTable(request);
                break;
            case 2:
                if (!$("#periode").val()) {
                    alert("PERIODE harus diisi.");
                    return;
                }
                defaultStartEnd();
                var pt = $("#select_pt").val();
                var request =
                    "?startmonth=" + startmonth + "&endmonth=" + endmonth + "&pt=" + pt;
                resetChart();
                getReportChart(request);
                break;
            case 4:
                if (!$("#periode").val()) {
                    alert("PERIODE harus diisi.");
                    return;
                }
                defaultStartEnd();
                var request =
                    "?startmonth=" + startmonth + "&endmonth=" + endmonth;
                resetTable();
                getOvertime(request);
                break;
            case 5:
                if (!$("#periode").val()) {
                    alert("PERIODE harus diisi.");
                    return;
                }
                defaultStartEnd();
                var pt = $("#select_pt").val();
                var request =
                    "?startmonth=" + startmonth + "&endmonth=" + endmonth + "&pt=" + pt;
                resetChart();
                getReportChartRapel(request);
                break;
            default:
                break;
        }
    });
}

const buttonSearchOvt = () => {
    $("#searchrapel").click(function () {
        switch (searchID) {
            case 3:
                if (!$("#periode").val()) {
                    alert("PERIODE harus diisi.");
                    return;
                }
                defaultStartEnd();
                var pt = $("#select_pt").val();
                var request =
                    "?startmonth=" + startmonth + "&endmonth=" + endmonth + "&pt=" + pt;
                resetTable();
                getReportTableRapel(request);
                break;
            case 4:
                if (!$("#periode").val()) {
                    alert("PERIODE harus diisi.");
                    return;
                }
                defaultStartEnd();
                var request =
                    "?startmonth=" + startmonth + "&endmonth=" + endmonth;
                resetTable();
                getOvertimeRapel(request);
                break;
            default:
                break;
        }
    });
}

const getReportTable = (url) => {
    url_data = "/getdatatable" + url;
    $.ajax({
        type: "GET",
        url: url_data,
        beforeSend: function () {},
        complete: function () {},
        success: function (data) {
            if (data != "") {
                processDataForTable(data);
            } else {
                alert("Tidak ada data.");
            }
        }
    });
}

const getReportTableRapel = (url) => {
    url_data = "/getdatatablerapel" + url;
    $.ajax({
        type: "GET",
        url: url_data,
        beforeSend: function () {},
        complete: function () {},
        success: function (data) {
            if (data != "") {
                processDataForTable(data);
            } else {
                alert("Tidak ada data.");
            }
        }
    });
}

const getOvertime = (url) => {
    url_data = "/getovertime" + url;

    $.ajax({
        type: "GET",
        url: url_data,
        beforeSend: function () {},
        complete: function () {},
        success: function (data) {
            if (data != "") {
                processDataForOvertime(data);
            } else {
                alert("Tidak ada data.");
            }
        }
    });
}

const getOvertimeRapel = (url) => {
    url_data = "/getovertimerapel" + url;

    $.ajax({
        type: "GET",
        url: url_data,
        beforeSend: function () {},
        complete: function () {},
        success: function (data) {
            if (data != "") {
                processDataForOvertime(data);
            } else {
                alert("Tidak ada data.");
            }
        }
    });
}

const getReportChart = (url) => {
    url_data = "/getdatachart" + url;

    $.ajax({
        type: "GET",
        url: url_data,
        beforeSend: function () {},
        complete: function () {},
        success: function (data) {
            if (data != "") {
                processDataForChart(data);
                drawChart(displayed_dept, overtimeArray);
                displayed_dept = new Array();
            } else {
                alert("Tidak ada data.");
            }
        }
    });
}

const getReportChartRapel = (url) => {
    url_data = "/getdatachartrapel" + url;

    $.ajax({
        type: "GET",
        url: url_data,
        beforeSend: function () {},
        complete: function () {},
        success: function (data) {
            if (data != "") {
                processDataForChart(data);
                drawChart(displayed_dept, overtimeArray);
                displayed_dept = new Array();
            } else {
                alert("Tidak ada data.");
            }
        }
    });
}

/* process all data for table */
const processDataForTable = (params) => {
    showTable(params);

    /* get only data */
    let oriJson = params.data;

    /* now loop for all table */
    $.each(oriJson, function (tableindex, value) {
        $("#tablebody_1")
            .append($("<tr />").attr("id"))
            .append($("<tr/>").attr("id", "bodytr_" + tableindex));

        $("#bodytr_" + tableindex)
            .append($("<td />", {
                text: moment(value.day).format('DD/MM/YYYY')
            }))
            .append($("<td/>", {
                text: value.golongan
            }))
            .append($("<td/>", {
                text: value.nik
            }))
            .append($("<td/>", {
                text: value.nama
            }))
            .append($("<td/>", {
                text: value.start
            }))
            .append($("<td/>", {
                text: value.finish
            }))
            .append($("<td/>", {
                text: value.istirahat
            }))
    });
}

const showTable = (params) => {
    let temp;

    temp = document.getElementsByTagName("template")[0];
    item = temp.content.querySelector(".box");

    tablecontent = document.importNode(item, true);
    /* create title */
    $(tablecontent)
        .find("h3")
        .html(
            "Report Table Overtime" +
            "</strong> <span class='badge bg-light-blue'>Dari " +
            params.header.startmonth +
            " s/d " +
            params.header.endmonth +
            " " +
            params.header.tahun +
            "</span>"
        );

    $(tablecontent)
        .find("table")
        .append($("<thead/>").attr("id", "tablethead_1"));

    $(tablecontent)
        .find("#tablethead_1")
        .append($("<tr/>").attr("id", "tabletr_1"));

    $(tablecontent)
        .find("#tabletr_1")
        .append($("<th />", {
            text: "Tgl Absen"
        }))
        .append($("<th />", {
            text: "K / L"
        }))
        .append($("<th />", {
            text: "NIK"
        }))
        .append($("<th />", {
            text: "Nama"
        }))
        .append($("<th />", {
            text: "st_ot2"
        }))
        .append($("<th />", {
            text: "fn_ot2"
        }))
        .append($("<th />", {
            text: "Rest"
        }))

    $(tablecontent)
        .find("table")
        .append($("<tbody/>").attr("id", "tablebody_1"));

    $("#contenttable").append(tablecontent);
}

const processDataForOvertime = (params) => {
    showTableOvertime(params);

    /* get only data */
    let oriJson = params.data;
    data_ovt = [];
    data_ovt.push(oriJson);
    let forNumber = 1;

    /* now loop for all table */
    $.each(oriJson, function (tableindex, value) {
        $("#tablebody_1")
            .append($("<tr />").attr("id"))
            .append($("<tr/>").attr("id", "bodytr_" + tableindex));

        $("#bodytr_" + tableindex)
            .append($("<td />", {
                text: forNumber
            }))
            .append($("<td />", {
                text: value.nik
            }))
            .append($("<td/>", {
                text: value.nama
            }))
            .append($("<td/>", {
                text: moment(value.day).format('DD/MM/YYYY')
            }))
            .append($("<td/>", {
                text: value.jam_mulai
            }))
            .append($("<td/>", {
                text: value.jam_akhir
            }))
            .append($("<td/>", {
                text: value.jam_kerja
            }))
            .append($("<td/>", {
                text: value.overtime
            }).attr("id", "hitOvertime" + value.id))
            // .append($("<td/>", {
            //     text: value.ovt
            // }).attr("id", "hitOvt" + value.id))
            .append($("<td/>", {
                text: value.status
            }))
            .append($("<td/>").append($("<a class='btn btn-warning btn-xs' href='javascript:void(0)' id='hitungovt'><i class='fa fa-business-time'></i> Ovt</a>").attr("data-id", value.id))
                .append($("<td/>").append($("<a class='btn btn-info btn-xs' href='javascript:void(0)' id='edit-user'><i class='fa fa-edit'></i> Edit</a>").attr("data-id", value.id))))
        forNumber++;
    });
}

const justOvt = () => {
    $('body').on('click', '#hitungovt', function () {
        const id = $(this).data('id');
        $.get('calcovt/' + id, function (data) {
            $("#hitOvertime" + id).html(data.overtime);
            // $("#hitOvt" + id).html(data.ovt);
        })
    });
}

const showTableOvertime = (params) => {
    let temp;

    temp = document.getElementsByTagName("template")[0];
    item = temp.content.querySelector(".box");

    tablecontent = document.importNode(item, true);
    /* create title */
    $(tablecontent)
        .find("h3")
        .html(
            params.header.jenis +
            "</strong> <span class='badge bg-light-blue'>Dari " +
            params.header.startmonth +
            " s/d " +
            params.header.endmonth +
            " " +
            params.header.tahun +
            "</span>"
        );

    $(tablecontent)
        .find("table")
        .append($("<thead/>").attr("id", "tablethead_1"));

    $(tablecontent)
        .find("#tablethead_1")
        .append($("<tr/>").attr("id", "tabletr_1"));

    $(tablecontent)
        .find("#tabletr_1")
        .append($("<th />", {
            text: "No"
        }))
        .append($("<th />", {
            text: "NIK"
        }))
        .append($("<th />", {
            text: "Nama"
        }))
        .append($("<th />", {
            text: "Tanggal"
        }))
        .append($("<th />", {
            text: "Masuk"
        }))
        .append($("<th />", {
            text: "Keluar"
        }))
        .append($("<th />", {
            text: "Shift"
        }))
        .append($("<th />", {
            text: "Ovt"
        }))
        // .append($("<th />", {
        //     text: "Index Overtime"
        // }))
        .append($("<th />", {
            text: "Status"
        }))
        .append($("<th />", {
            text: "Action"
        }));

    $(tablecontent)
        .find("table")
        .append($("<tbody/>").attr("id", "tablebody_1"));

    $("#contenttable").append(tablecontent);
}

/* process all data for chart */
const processDataForChart = (params) => {
    var oriJson = params.data;

    var byDept = groupBy(oriJson, "dept");
    $.each(byDept, function (index) {
        displayed_dept.push(index);
    });
    const deptlist = Object.keys(byDept);
    overtimeArray = [];
    deptlist.forEach((dept) => {
        overtimeArray.push(byDept[dept].reduce(
            (acc, el) => acc + parseInt(el.overtime), 0
        ))
    });
}

/* charting */
const drawChart = (dept, total_data_overtime) => {
    const areaChartData = {
        labels: dept,
        datasets: [{
            label: "Total Overtime",
            backgroundColor: "#5333ed",
            data: total_data_overtime
        }]
    };
    const areaChartOptions = {
        scales: {
            yAxes: [{
                display: true,
                ticks: {
                    beginAtZero: true,
                    max: 40,
                    min: 0,
                    stepSize: 2
                }
            }]
        }
    };

    const areaChartCanvas = $("#areaChart")
        .get(0)
        .getContext("2d");
    const areaChart = new Chart(areaChartCanvas, {
        type: "bar",
        data: areaChartData,
        options: areaChartOptions
    });
}

const resetTable = () => {
    $("#contenttable").empty();
}

const resetChart = () => {
    var canvas = document.getElementById("areaChart");
    var ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

/* helper grouping */
const groupBy = (xs, key) => {
    return xs.reduce(function (rv, x) {
        (rv[x[key]] = rv[x[key]] || []).push(x);
        return rv;
    }, {});
};

const defaultStartEnd = () => {
    let datenow = moment().format("YYYY-MM");
    if (startmonth == undefined) startmonth = datenow;
    if (endmonth == undefined) endmonth = datenow;
}

const ajaxEdit = () => {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('body').on('click', '#edit-user', function () {
        $('#select_shift').children().remove();
        const id = $(this).data('id');
        $.get('/getoneovertime/' + id, function (data) {
            $('#userCrudModal').html("Edit Overtime");
            $('#btn-save').val("edit-user");
            $('#ajax-crud-modal').modal('show');
            $('#id').val(data.id);
            $('#nik').val(data.nik);
            $('#nama').val(data.nama);
            $('#deptsect').val(data.dept + data.sect);
            $('#tanggal').val(moment(data.day).format('DD/MM/YYYY'));
            $('#select_shift').append($('<option>').val(data.jam_kerja).text(data.jam_kerja));
            $('#in').val(data.jam_mulai);
            $('#out').val(data.jam_akhir);
            $('#overtime').val(data.overtime);
            $('#idxovt').val(data.ovt);
            $('#stot1').val(data.star_ot2);
            $('#gol').val(data.golongan);
            $('#status').val(data.status);
            $('#ist').val(data.istirahat);
            $('#ins').val(data.insentif);
            $('#fnot1').val(data.roundtime);
            if (data.fnot) $('#fnot1').val(data.fnot);
            $('#ovt_activity').val(data.ovt_activity);
            $('#ovt_type').val(data.ovt_type).change();
            $('#ket').val(data.keterangan);
            $('#stot').val(data.stot);
            $('#fnot').val(data.fnot);
            $('#rapel').val(data.rapel ? moment(data.rapel).format('DD-MM-YYYY') : '');
        })
    });
    if ($("#userForm").length > 0) {
        $("#userForm").validate({
            submitHandler: function (form) {
                const id = $('#id').val();
                $('#btn-save').html('Sending..');
                $.ajax({
                    data: $('#userForm').serialize(),
                    url: "updateovertime/" + id,
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        var request =
                            "?startmonth=" + startmonth + "&endmonth=" + endmonth;
                        resetTable();
                        getOvertime(request);
                        $('#userForm').trigger("reset");
                        $('#ajax-crud-modal').modal('hide');
                        $('#btn-save').html('Save Changes');
                    },
                    error: function (data) {
                        $('#btn-save').html('Save Changes');
                    }
                });
            }
        })
    }
}

const selectShift = () => {
    $("#select_shift").select2({
        ajax: {
            url: "/getshift",
            dataType: "json",
            delay: 100,
            data: function (params) {
                return {
                    q: params.term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.shift_id,
                            id: item.shift_id
                        }
                    })
                };
            },
            cache: true
        },
        placeholder: "Pilih Shift",
        width: '100%'
    });
}

const selectStatus = () => {
    $("#select_status").select2({
        ajax: {
            url: "/getstatus",
            dataType: "json",
            delay: 100,
            data: function (params) {
                return {
                    q: params.term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.name
                        }
                    })
                };
            },
            cache: true
        },
        placeholder: "Pilih Status",
        width: '100%'
    });
}

/*
 * OnLoading
 * ------------------
 */

jQuery(document).ready(function ($) {

    if (location.href.indexOf("login") == -1) {
        getRole();
    }

    if (location.href.indexOf("reporttable") > -1) {
        searchID = 1;
        selectPeriode();
        registerButtonSearch();
        registerButtonExcel();
        registerButtonPrint();
    }

    if (location.href.indexOf("reportbar") > -1) {
        searchID = 2;
        selectPeriode();
        registerButtonSearch();
        registerButtonPrint();
    }

    if (location.href.indexOf("reportrapel") > -1) {
        searchID = 3;
        selectPeriode();
        buttonSearchOvt();
        registerButtonExcel();
        registerButtonPrint();
    }

    if (location.href.indexOf("dashboard") > -1) {
        searchID = 4;
        selectShift();
        selectStatus();
        selectPeriode();
        registerButtonSearch();
        buttonSearchOvt();
        ajaxEdit();
        justOvt();
    }

    if (location.href.indexOf("reportbarrapel") > -1) {
        searchID = 5;
        selectPeriode();
        registerButtonSearch();
        registerButtonPrint();
    }
});
