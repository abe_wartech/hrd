$(document).ready(function () {
    $("#select_karyawan").select2({
        ajax: {
            url: "/getkaryawan",
            dataType: "json",
            delay: 100,
            data: function (params) {
                return {
                    q: params.term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.nama,
                            id: item.nik
                        }
                    })
                };
            },
            cache: true
        },
        placeholder: "Pilih Karyawan",
        width: '100%'
    });
    $(document.body).on("change", "#select_karyawan", function () {
        $.ajax({
            url: "/getonekaryawan/" + this.value,
            dataType: "json",
            success: function (response) {
                $.map(response, function (item) {
                    $('#nik').val(item.nik);
                    $('#nama').val(item.nama);
                    $('#dept').val(item.dept + " / " + item.sect);
                })
            },
        });
    });
    $(document.body).on("change", "#select_shift", function () {
        $.ajax({
            url: "/getoneshift/" + this.value,
            dataType: "json",
            success: function (response) {
                $.map(response, function (item) {
                    if (item.star_ot == '') {
                        $('#stot').val(item.endtime);
                    } else {
                        $('#stot').val(item.star_ot);
                    }
                })
            },
        });
    });
    $(function () {
        moment.locale('id');
        $('#tanggal').daterangepicker({
            singleDatePicker: true,
            locale: {
                format: 'DD-MM-YYYY'
            }
        });
        $('#rapel').daterangepicker({
            singleDatePicker: true,
            autoUpdateInput: false,
            locale: {
                format: 'DD-MM-YYYY'
            }
        }).on("apply.daterangepicker", function (e, picker) {
            picker.element.val(picker.startDate.format(picker.locale.format));
        });
        $('#in').datetimepicker({
            format: 'HH:mm'
        });
        $('#out').datetimepicker({
            format: 'HH:mm'
        });
        $('#stot').datetimepicker({
            format: 'HH:mm'
        });
        $('#fnot').datetimepicker({
            format: 'HH:mm'
        });
    });
    $(".input-file").before(
        function () {
            if (!$(this).prev().hasClass('input-ghost')) {
                var element = $("<input type='file' name='absen' class='input-ghost' style='visibility:hidden; height:0'>");
                element.attr("name", $(this).attr("name"));
                element.change(function () {
                    element.next(element).find('input').val((element.val()).split('\\').pop());
                });
                $(this).find("button.btn-choose").click(function () {
                    element.click();
                });
                $(this).find("button.btn-reset").click(function () {
                    element.val(null);
                    $(this).parents(".input-file").find('input').val('');
                });
                $(this).find('input').css("cursor", "pointer");
                $(this).find('input').mousedown(function () {
                    $(this).parents('.input-file').prev().click();
                    return false;
                });
                return element;
            }
        }
    );
});
