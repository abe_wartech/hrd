FROM php:7.1.27-fpm-jessie

RUN apt-get update && apt-get install -y libmcrypt-dev libjpeg62-turbo-dev libfreetype6-dev libpng-dev zlib1g-dev vim \
    mysql-client --no-install-recommends sudo nano \
    && docker-php-ext-install mcrypt pdo_mysql \
    && docker-php-ext-install zip \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install gd

RUN apt-get install -y openssl zip unzip git
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

#install Imagemagick & PHP Imagick ext
RUN apt-get install -y \
    --no-install-recommends libmagickwand-dev \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN pecl install imagick && docker-php-ext-enable imagick

# add npm
RUN curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
RUN ln -s /usr/bin/nodejs /usr/bin/node && alias node=nodejs

RUN sudo apt-get install -y --force-yes nodejs
RUN npm install -g npm@latest || npm install -g npm@latest

RUN docker-php-ext-install pdo mbstring

RUN apt-get update && apt-get install -y cron && \
    rm -r /var/lib/apt/lists/*

WORKDIR /app
COPY . /app

RUN composer install
RUN npm install