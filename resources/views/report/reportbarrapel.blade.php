@extends('backpack::layout')
@section('header')
<section class="content-header">
    <h1>
        {{ 'Report Bar Overtime Rapel' }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">{{ 'Report Bar Overtime Rapel' }}</li>
    </ol>
</section>
@endsection
@section('content')
<div class="box">
    <div class="box-header with-border">
        <div class="box-body">
            <div class="row">
                <div class="col-xs-1">
                    <h5>PERIODE</h5>
                </div>
                <div class="col-xs-3">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control" id="periode" autocomplete="off"/>
                        </div>
                    </div>
                </div>
                <div class="col-xs-1 m-l-20">
                    <h5>PT</h5>
                </div>
                <div class="col-xs-3" style="margin-left: -4%">
                    <div class="form-group">
                        <div class="input-group">
                            <select class="form-control" id="select_pt">
                                <option value="CNG,CNE">All (CNG & CNE)</option>
                                <option value="CNG">CNG</option>
                                <option value="CNE">CNE</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-success" id="search"><i class="fa fa-search"></i> Cari</button>
                    <button type="button" class="btn btn-warning" id="print"><i class="fa fa-print"></i> Cetak</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Report Bar Overtime</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <div class="chart">
            <canvas id="areaChart" width="639" height="315"></canvas>
        </div>
    </div>
</div>
@endsection
