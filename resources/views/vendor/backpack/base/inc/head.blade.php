<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
@if (config('backpack.base.meta_robots_content'))
<meta name="robots" content="{{ config('backpack.base.meta_robots_content', 'noindex, nofollow') }}">
@endif

{{-- Encrypted CSRF token for Laravel, in order for Ajax requests to work --}}
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
<title>
    {{ isset($title) ? $title.' :: '.config('backpack.base.project_name').' Admin' : config('backpack.base.project_name').' Admin' }}
</title>

@yield('before_styles')
@stack('before_styles')

<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="stylesheet" href="{{ asset(mix('css/hasil_combine.css')) }}">
<link rel="stylesheet" href="{{ asset('vendor/adminlte/') }}/bower_components/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="{{ asset('vendor/adminlte/') }}/bower_components/font-awesome/fontawesome-5.10.2/css/all.min.css">
<link rel="stylesheet" href="{{ asset('vendor/custom/TableExport/css/tableexport.min.css') }}">
@if (config('backpack.base.overlays') && count(config('backpack.base.overlays')))
@foreach (config('backpack.base.overlays') as $overlay)
<link rel="stylesheet" href="{{ asset($overlay) }}">
@endforeach
@endif


@yield('after_styles')
@stack('after_styles')
