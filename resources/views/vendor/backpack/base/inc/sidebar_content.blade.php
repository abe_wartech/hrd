<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
@if(backpack_user()->hasRole('HRD'))
<li class="treeview">
    <a href="#"><i class="fa fa-group"></i> <span>Users Management</span> <i
            class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('users') }}"><i class="fa fa-user"></i> <span>Users</span></a></li>
        <!-- <li><a href="{{ backpack_url('role') }}"><i class="fa fa-group"></i> <span>Jabatan</span></a></li> -->
    </ul>
</li>
<li class="treeview">
    <a href="#"><i class="fa fa-file-archive-o"></i> <span>Master</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('area') }}"><i class="fa fa-building"></i> <span>Area</span></a></li>
        <li><a href="{{ backpack_url('karyawan') }}"><i class="fa fa-user-tie"></i> <span> Karyawan</span></a></li>
        <li><a href="{{ backpack_url('karyawanbiodata') }}"><i class="fa fa-user-tag"></i> <span>Karyawan
                    Biodata</span></a></li>
        <li><a href="{{ backpack_url('shift') }}"><i class="fa fa-business-time"></i> <span>Shift</span></a></li>
        <li><a href="{{ backpack_url('status') }}"><i class="fa fa-clipboard-list"></i> <span>Status</span></a></li>
        <li><a href="{{ backpack_url('insentif') }}"><i class="fa fa-money-bill-alt"></i> <span>Insentif</span></a></li>
    </ul>
</li>
<li class="treeview">
    <a href="#"><i class="fa fa-print"></i> <span>Report</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('reporttable') }}"><i class="fa fa-table"></i> <span>Report Table</span></a></li>
        <li><a href="{{ backpack_url('reportbar') }}"><i class="fa fa-bar-chart"></i> <span>Report Bar</span></a></li>

        </li>
    </ul>
</li>
<li class="treeview">
    <a href="#"><i class="fa fa-chart-pie"></i> <span>Report Rapel</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('reportrapel') }}"><i class="fa fa-table"></i> <span>Report Table Rapel</span></a></li>
        <li><a href="{{ backpack_url('reportbarrapel') }}"><i class="fa fa-bar-chart"></i> <span>Report Bar Rapel</span></a></li>
        </li>
    </ul>
</li>
@endif
@if(!backpack_user()->hasRole('HRD|Staff'))
<li class="treeview">
    <a href="#"><i class="fa fa-print"></i> <span>Report</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('reporttable') }}"><i class="fa fa-table"></i> <span>Report Table</span></a></li>
        <li><a href="{{ backpack_url('reportbar') }}"><i class="fa fa-bar-chart"></i> <span>Report Bar</span></a></li>

        </li>
    </ul>
</li>
@endif
