@extends('backpack::layout')

@section('header')
<section class="content-header">
    <h1>
        {{ trans('backpack::base.dashboard') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">{{ trans('backpack::base.dashboard') }}</li>
    </ol>
</section>
@endsection

@section('content')
<div class="box">
    <div class="box-header with-border">
        <div class="box-body">
            <div class="row">
                <div class="col-xs-1">
                    <h5>PERIODE</h5>
                </div>
                <div class="col-xs-3">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control" id="periode" autocomplete="off" />
                        </div>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="btn-group">
                        <button type="button" class="btn btn-success" id="search">
                            <i class="fas fa-business-time"></i> Overtime</button>
                        <button type="button" class="btn btn-warning" id="searchrapel">
                            <i class="fas fa-calendar-alt"></i> Overtime Rapel</button>
                    </div>
                </div>
                <div class="col-xs-1">
                    <h5>ABSEN</h5>
                </div>
                <div class="col-xs-4" style="margin-left: -20px">
                    <div class="form-group">
                        <form role="form" action="{{route('absen')}}" method="POST" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <div class="input-group input-file">
                                <div class="input-group-addon">
                                    <i class="fa fa-file-excel"></i>
                                </div>
                                <input type="text" class="form-control" />
                                <span class="input-group-btn">
                                    <button class="btn btn-default btn-choose" style="height: 34px !important"
                                        type="button">Browse</button>
                                    <button type="submit" class="btn btn-default" style="height: 34px !important"
                                        type="button">Upload</button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div id="contenttable"></div>
<!-- table template -->
<template>
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                        class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body table-responsive">
            <table class="table table-bordered table-hover">
            </table>
        </div>
    </div>
</template>
<div class="modal fade" id="ajax-crud-modal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="userForm" name="userForm" class="form-horizontal" autocomplete="off">
                <div class="modal-header">
                    <h4 class="modal-title" id="userCrudModal"></h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-4 m-r-10">
                                <input type="hidden" id="id">
                                <div class="form-group">
                                    <label>NIK</label>
                                    <input type="text" class="form-control" id="nik" value="" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" class="form-control" id="nama" value="" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Dept. / Sect.</label>
                                    <input type="text" id="deptsect" class="form-control" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Golongan</label>
                                    <select class="form-control" name="gol" id="gol">
                                        <option value="kerja">Kerja</option>
                                        <option value="libur6">Libur 6</option>
                                        <option value="libur5">Libur 5</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Shift</label>
                                    <select id="select_shift" class="form-control" name="shift">
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Status</label>
                                    <select id="select_status" class="form-control" name="status"></select>
                                </div>
                                <div class="form-group">
                                    <label>Ovt Type</label>
                                    <select class="form-control" name="ovt_type" id="ovt_type">
                                        <option value=""></option>
                                        <option value="rutin">Rutin</option>
                                        <option value="nonrutin">Non Rutin</option>
                                        <option value="project">Project</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 m-r-10">
                                <input type="hidden" name="id" id="id">
                                <div class="form-group">
                                    <label>Tanggal</label>
                                    <input type="text" class="form-control" id="tanggal" name="tanggal" disabled>
                                </div>
                                <div class="form-group">
                                    <label>IN</label>
                                    <input type="text" class="form-control" id="in" value="" name="jam_mulai">
                                </div>
                                <div class="form-group">
                                    <label>OUT</label>
                                    <input type="text" class="form-control" id="out" value="" name="jam_akhir">
                                </div>
                                <div class="form-group">
                                    <label>St Ovt</label>
                                    <input type="text" id="stot" name="stot" class="form-control" >
                                </div>
                                <div class="form-group">
                                    <label>Istirahat</label>
                                    <input type="number" id="ist" class="form-control" name="istirahat" required>
                                </div>
                                <div class="form-group">
                                    <label>Keterangan</label>
                                    <input type="text" id="ket" class="form-control" name="ket">
                                </div>
                                <div class="form-group">
                                    <label>Ovt Activity</label>
                                    <input type="text" id="ovt_activity" class="form-control" name="ovt_activity"
                                        >
                                </div>
                            </div>
                            <div class="col-md-35">
                                <input type="hidden" name="id" id="id">
                                <div class="form-group">
                                    <label>St Ot 1</label>
                                    <input type="text" id="stot1" class="form-control" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Fn Ot 1</label>
                                    <input type="text" id="fnot1" class="form-control" disabled>
                                </div>
                                <div class="form-group">
                                    <label>Rapel Overtime</label>
                                    <input type="text" id="rapel" class="form-control" name="rapel">
                                </div>
                                <div class="form-group">
                                    <label>Fn Ovt</label>
                                    <input type="text" id="fnot" name="fnot" class="form-control" >
                                </div>
                                <div class="form-group">
                                    <label>Insentif</label>
                                    <input type="text" id="ins" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>OVT</label>
                                    <input type="text" class="form-control" id="overtime" value="" disabled>
                                </div>
                                <!-- <div class="form-group">
                                    <label>Index OVT</label>
                                    <input type="text" class="form-control" id="idxovt" value="" disabled>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="btn-save" value="create">Save changes
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
