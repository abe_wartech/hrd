<?php

Route::redirect('/', 'admin');
Route::redirect('/home', 'admin/dashboard');
Route::group(['prefix' => 'admin', 'middleware' => ['role:HRD']], function () {
    CRUD::resource('users', 'Admin\UsersCrudController');
    CRUD::resource('area', 'Admin\AreaCrudController');
    CRUD::resource('role', 'Admin\RoleCrudController');
    CRUD::resource('status', 'Admin\StatusCrudController');
    CRUD::resource('insentif', 'Admin\InsentifCrudController');
    Route::get('getrole', 'HelperController@GetRole');
    CRUD::resource('karyawan', 'Admin\KaryawanCrudController');
    CRUD::resource('karyawanbiodata', 'Admin\KaryawanBiodataCrudController');
    CRUD::resource('shift', 'Admin\ShiftCrudController');
    Route::post('absen', 'OvertimeController@absen')->name('absen');
    Route::get('reporttable', 'ReportController@ReportTable');
    Route::get('reportbar', 'ReportController@ReportBar');
    Route::get('reportrapel', 'ReportController@ReportTableRapel');
    Route::get('reportbarrapel', 'ReportController@ReportBarRapel');
    Route::post('updateovertime/{id}', 'HelperController@UpdateOvertime');
    Route::get('calcovt/{id}', 'HelperController@CalcOvertime');
});
Route::get('getarea', 'HelperController@GetWilayah');
Route::get('getstatus', 'HelperController@GetStatus');
Route::get('getdatatable', 'ReportController@GetDataForTable');
Route::get('getdatachart', 'ReportController@GetDataForChart');
Route::get('getdatachartrapel', 'ReportController@GetDataForChartRapel');
Route::get('getdatatablerapel', 'ReportController@GetDataForTableRapel');
Route::get('getovertime', 'HelperController@GetDataForOvertime');
Route::get('getovertimerapel', 'HelperController@GetDataForOvertimeRapel');
Route::get('getkaryawan', 'HelperController@GetDataKaryawan');
Route::get('getshift', 'HelperController@GetDataShift');
Route::get('getoneshift/{id}', 'HelperController@GetOneDataShift');
Route::get('getonekaryawan/{id}', 'HelperController@GetOneDataKaryawan');
Route::get('getoneovertime/{id}', 'HelperController@GetOneDataForOvertime');
