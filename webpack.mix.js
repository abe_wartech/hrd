const mix = require('laravel-mix');

mix.styles([
        'public/vendor/custom/bootstrap-3.4.1/css/bootstrap.min.css',
        'public/vendor/custom/ionicons-2.0.1/css/ionicons.min.css',
        'public/vendor/adminlte/dist/css/AdminLTE.min.css',
        'public/vendor/adminlte/dist/css/skins/_all-skins.min.css',
        'public/vendor/adminlte/plugins/pace/pace.min.css',
        'public/vendor/backpack/pnotify/pnotify.custom.min.css',
        'public/vendor/adminlte/plugins/timepicker/timepicker.css',
        'public/js/select2/css/select2.min.css',
        'public/vendor/backpack/base/backpack.base.css',
        'public/vendor/adminlte/bower_components/font-awesome/css/date.css',
    ],
    'public/css/hasil_combine.css').options({
    processCssUrls: false,
    cleanCss: {
        level: {
            1: {
                specialComments: 'none'
            }
        }
    },
    postCss: [require('postcss-discard-comments')({
        removeAll: true
    })]
}).version();

mix.scripts([
        'public/vendor/adminlte/bower_components/jquery/dist/jquery.min.js',
        'public/vendor/custom/bootstrap-3.4.1/js/bootstrap.min.js',
        'public/vendor/adminlte/plugins/pace/pace.min.js',
        'public/vendor/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js',
        'public/vendor/adminlte/dist/js/adminlte.js',
        'public/js/helper.js',
        'public/js/custom.js',
        'public/vendor/adminlte/bower_components/moment/min/moment-with-locales.min.js',
        'public/js/select2/js/select2.min.js',
        'public/vendor/custom/bootstrap-3.4.1/js/bootstrap-datetimepicker.min.js',
        'public/vendor/custom/daterangepicker-master/daterangepicker.js',
        'public/vendor/custom/daterangepicker-master/Chart.min.js',
        'public/vendor/custom/daterangepicker-master/jquery.validate.js',
        'public/vendor/custom/js-xlsx/js/xlsx.core.min.js',
        'public/vendor/custom/FileSaver/js/FileSaver.min.js',
        'public/vendor/custom/TableExport/js/tableexport.min.js',
    ],
    'public/js/hasil_combine.js').options({
    processCssUrls: false,
    cleanCss: {
        level: {
            1: {
                specialComments: 'none'
            }
        }
    },
    postCss: [require('postcss-discard-comments')({
        removeAll: true
    })]
}).version();

mix.browserSync({
    proxy: 'http://localhost:8000/',
    files: ["public/css/*.css", "public/vendor/backpack/base/*.css", , "resources/views/vendor/backpack/base/*.blade.php", "public/js/*.js", "resources/views/vendor/backpack/base/inc/*.blade.php"],
    notify: false
});
