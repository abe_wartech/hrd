FROM nginx:latest

ADD vhost.conf /etc/nginx/conf.d/default.conf

RUN apt-get update && apt-get install -y vim sudo nano
