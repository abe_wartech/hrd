<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Karyawan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('karyawan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nik')->unique();
            $table->string('pt')->nullable();
            $table->string('nama')->nullable();
            $table->string('dept')->nullable();
            $table->string('sect')->nullable();
            $table->string('status')->nullable();
            $table->string('jabatan')->nullable();
            $table->string('tgl_msuk')->nullable();
            $table->string('prob')->nullable();
            $table->string('tgl_keluar')->nullable();
            $table->string('no_paklaring')->nullable();
            $table->string('lokasi')->nullable();
            $table->string('tgl_lahir')->nullable();
            $table->string('tvb')->nullable();
            $table->string('dept_code')->nullable();
            $table->string('level')->nullable();
            $table->string('nikah')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('karyawan');
    }
}
