<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class KaryawanBiodata extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('karyawan_biodata', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('NIK')->unique();
            $table->string('NAMA')->nullable();
            $table->string('GENDER')->nullable();
            $table->string('TEMPAT_LHR')->nullable();
            $table->string('tgl_lahir')->nullable();
            $table->string('STATUS_PEGAWAI')->nullable();
            $table->string('NO_TLP')->nullable();
            $table->string('GOL')->nullable();
            $table->string('GRADE')->nullable();
            $table->string('TANGGAL_MASUK')->nullable();
            $table->string('TANGGAL_KELUAR')->nullable();
            $table->string('JABATAN')->nullable();
            $table->string('AGAMA')->nullable();
            $table->string('KEBANGSAAN')->nullable();
            $table->string('STATUS_NIKAH')->nullable();
            $table->string('JML_TANGGUNGAN')->nullable();
            $table->string('ALAMAT_TINGGAL')->nullable();
            $table->string('KOTA')->nullable();
            $table->string('ALAMAT_KTP')->nullable();
            $table->string('EMAIL')->nullable();
            $table->string('NO_KTP')->nullable();
            $table->string('PENDIDIKAN')->nullable();
            $table->string('JURUSAN')->nullable();
            $table->string('KANTOR/PLANT')->nullable();
            $table->string('BANK')->nullable();
            $table->string('CABANG_BANK')->nullable();
            $table->string('NO_REK')->nullable();
            $table->string('NPWP')->nullable();
            $table->string('TGL_PENGUKUHAN_NPWP')->nullable();
            $table->string('NO_JAMSOSTEK')->nullable();
            $table->string('TGL_MSK_JAMSOSTEK')->nullable();
            $table->string('ID_SO')->nullable();
            $table->string('GOL_DARAH')->nullable();
            $table->string('IBU_KANDUNG')->nullable();
            $table->string('MASA_KTP')->nullable();
            $table->string('AN_NOREK')->nullable();
            $table->string('KD_LBR')->nullable();
            $table->string('KD_ABS')->nullable();
            $table->string('PHOTO')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('karyawan_biodata');
    }
}
