<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Overtime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('overtime', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_id');
            $table->string('jam_mulai');
            $table->string('jam_akhir');
            $table->string('jam_kerja')->nullable();
            $table->string('status')->nullable();
            $table->string('overtime');
            $table->string('ovt');
            $table->string('day');
            $table->string('golongan')->nullable();
            $table->string('ovt_type')->nullable();
            $table->string('ovt_activity')->nullable();
            $table->string('keterangan')->nullable();
            $table->string('insentif')->default('0');
            $table->string('istirahat')->default('0');
            $table->string('stot')->nullable();
            $table->string('fnot')->nullable();
            $table->string('roundtime')->nullable();
            $table->string('start');
            $table->string('finish');
            $table->string('rapel')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('overtime');
    }
}
