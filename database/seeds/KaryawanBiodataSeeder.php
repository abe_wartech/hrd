<?php

use Illuminate\Database\Seeder;

class KaryawanBiodataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $karyawanbiodata = array(
            array('11111', 'AAAAAAAAAAAAAA', 'M', 'Sukabumi', '18/03/1973', 'Tetap', '', str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), "I6", str_random(10), str_random(10)),
            array('22222', 'BBBBBBBBBBBBBBBB', 'M', 'Brebes', '18/03/1978', 'Tetap', '', str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), "L5", str_random(10), str_random(10)),
            array('33333', 'CCCCCCCCCCCCCCCC', 'M', 'Jakarta', '18/03/1975', 'Tetap', '', str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), "I6", str_random(10), str_random(10)),
        );

        $karyawanbiodatacount = count($karyawanbiodata);

        for ($i = 0; $i < $karyawanbiodatacount; $i++) {
            DB::table('karyawan_biodata')->insert(array(
                'NIK' => $karyawanbiodata[$i][0],
                'NAMA' => $karyawanbiodata[$i][1],
                'GENDER' => $karyawanbiodata[$i][2],
                'TEMPAT_LHR' => $karyawanbiodata[$i][3],
                'tgl_lahir' => $karyawanbiodata[$i][4],
                'STATUS_PEGAWAI' => $karyawanbiodata[$i][5],
                'NO_TLP' => $karyawanbiodata[$i][6],
                'GOL' => $karyawanbiodata[$i][7],
                'GRADE' => $karyawanbiodata[$i][8],
                'TANGGAL_MASUK' => $karyawanbiodata[$i][9],
                'TANGGAL_KELUAR' => $karyawanbiodata[$i][10],
                'JABATAN' => $karyawanbiodata[$i][11],
                'AGAMA' => $karyawanbiodata[$i][12],
                'KEBANGSAAN' => $karyawanbiodata[$i][13],
                'STATUS_NIKAH' => $karyawanbiodata[$i][14],
                'JML_TANGGUNGAN' => $karyawanbiodata[$i][15],
                'ALAMAT_TINGGAL' => $karyawanbiodata[$i][16],

                'KOTA' => $karyawanbiodata[$i][17],
                'ALAMAT_KTP' => $karyawanbiodata[$i][18],
                'EMAIL' => $karyawanbiodata[$i][19],
                'NO_KTP' => $karyawanbiodata[$i][20],
                'PENDIDIKAN' => $karyawanbiodata[$i][21],
                'JURUSAN' => $karyawanbiodata[$i][22],
                'KANTOR/PLANT' => $karyawanbiodata[$i][23],
                'BANK' => $karyawanbiodata[$i][24],
                'CABANG_BANK' => $karyawanbiodata[$i][25],
                'NO_REK' => $karyawanbiodata[$i][26],
                'NPWP' => $karyawanbiodata[$i][27],
                'TGL_PENGUKUHAN_NPWP' => $karyawanbiodata[$i][28],
                'NO_JAMSOSTEK' => $karyawanbiodata[$i][29],
                'TGL_MSK_JAMSOSTEK' => $karyawanbiodata[$i][30],
                'ID_SO' => $karyawanbiodata[$i][31],
                'GOL_DARAH' => $karyawanbiodata[$i][32],
                'IBU_KANDUNG' => $karyawanbiodata[$i][33],
                'MASA_KTP' => $karyawanbiodata[$i][34],
                'AN_NOREK' => $karyawanbiodata[$i][35],
                'KD_LBR' => $karyawanbiodata[$i][36],
                'KD_ABS' => $karyawanbiodata[$i][37],
                'PHOTO' => $karyawanbiodata[$i][38],

                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ));
        }
    }
}
