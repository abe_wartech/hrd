<?php

use Illuminate\Database\Seeder;

class KaryawanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $karyawan = array(
            array('11111', 'CNG', 'AAAAAAAAAAAAAA', 'Production', 'Mtn Pro', 'Tetap', 'ST', str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10)),
            array('22222', 'CNE', 'BBBBBBBBBBBBBBBB', 'HRD & GA', 'GA', 'Tetap', 'ST', str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10)),
            array('33333', 'CNG', 'CCCCCCCCCCCCCCCC', 'Production', 'Mtn Pro', 'Tetap', 'SH', str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10), str_random(10)),
        );

        $karyawancount = count($karyawan);

        for ($i = 0; $i < $karyawancount; $i++) {
            DB::table('karyawan')->insert(array(
                'nik' => $karyawan[$i][0],
                'pt' => $karyawan[$i][1],
                'nama' => $karyawan[$i][2],
                'dept' => $karyawan[$i][3],
                'sect' => $karyawan[$i][4],
                'status' => $karyawan[$i][5],
                'jabatan' => $karyawan[$i][6],
                'tgl_msuk' => $karyawan[$i][7],
                'prob' => $karyawan[$i][8],
                'tgl_keluar' => $karyawan[$i][9],
                'no_paklaring' => $karyawan[$i][10],
                'lokasi' => $karyawan[$i][11],
                'tgl_lahir' => $karyawan[$i][12],
                'tvb' => $karyawan[$i][13],
                'dept_code' => $karyawan[$i][14],
                'level' => $karyawan[$i][15],
                'nikah' => $karyawan[$i][16],
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ));
        }
    }
}
