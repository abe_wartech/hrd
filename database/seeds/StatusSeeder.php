<?php

use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = array(
            array('Non Clocking'),
            array('Datang Terlambat'),
            array('Offline'),
        );

        $statuscount = count($status);

        for ($i = 0; $i < $statuscount; $i++) {
            DB::table('status')->insert(array(
                'name' => $status[$i][0],
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ));
        }
    }
}
