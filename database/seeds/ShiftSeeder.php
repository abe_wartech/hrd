<?php

use Illuminate\Database\Seeder;
use Triasrahman\JSONSeeder\JSONSeeder;

class ShiftSeeder extends Seeder
{
    use JSONSeeder;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->JSONSeed('shift', '\App\Models\Shift');
    }
}
