<?php

use Illuminate\Database\Seeder;

class AreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $area = array(
            array('Jakarta'),
            array('Bogor'),
            array('Surabaya'),
            array('Semarang'),
        );

        $areacount = count($area);

        for ($i = 0; $i < $areacount; $i++) {
            DB::table('area')->insert(array(
                'name' => $area[$i][0],
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ));
        }
    }
}
