<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(AreaSeeder::class);
        $this->call(KaryawanSeeder::class);
        $this->call(KaryawanBiodataSeeder::class);
        $this->call(ShiftSeeder::class);
        $this->call(StatusSeeder::class);
        $this->call(InsentifSeeder::class);
    }
}
