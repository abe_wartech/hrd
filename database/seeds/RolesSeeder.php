<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $config = config('permission.table_names');

        DB::table($config['roles'])->insert(array(
            //HRD
            array('name' => 'HRD', 'guard_name' => 'api', 'created_at' => new DateTime,
                'updated_at' => new DateTime),
            //Staff
            array('name' => 'Staff', 'guard_name' => 'api', 'created_at' => new DateTime,
                'updated_at' => new DateTime),
            //Sect. Head
            array('name' => 'SH', 'guard_name' => 'api', 'created_at' => new DateTime,
                'updated_at' => new DateTime),
            //Dept. Head
            array('name' => 'DH', 'guard_name' => 'api', 'created_at' => new DateTime,
                'updated_at' => new DateTime),
            //Div. Head
            array('name' => 'DV', 'guard_name' => 'api', 'created_at' => new DateTime,
                'updated_at' => new DateTime),
            //General Manager
            array('name' => 'GM', 'guard_name' => 'api', 'created_at' => new DateTime,
                'updated_at' => new DateTime),
            //Direksi
            array('name' => 'Direksi', 'guard_name' => 'api', 'created_at' => new DateTime,
                'updated_at' => new DateTime),
        ));
    }
}
