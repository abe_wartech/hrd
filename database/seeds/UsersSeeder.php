<?php

use App\Models\BackpackUser as User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //HRD
        $user1 = User::create([
            'name' => 'Account HRD',
            'nik' => '3201011308980011',
            'password' => Hash::make('123456'),
            'golongan' => 'kerja',
            'jabatan_id' => '1',
            'area_id' => '1',
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $user1->assignRole('HRD');
        //Staff
        $user2 = User::create([
            'name' => 'Account Staff',
            'nik' => '3201011308980012',
            'password' => Hash::make('123456'),
            'golongan' => 'kerja',
            'jabatan_id' => '2',
            'area_id' => '1',
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $user2->assignRole('Staff');
        //Staff Libur5
        $user3 = User::create([
            'name' => 'Account Staff Libur 5',
            'nik' => '3201011308980022',
            'password' => Hash::make('123456'),
            'golongan' => 'libur5',
            'jabatan_id' => '2',
            'area_id' => '1',
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $user3->assignRole('Staff');
        //Staff
        $user4 = User::create([
            'name' => 'Account Staff Libur 6',
            'nik' => '3201011308980032',
            'password' => Hash::make('123456'),
            'golongan' => 'libur6',
            'jabatan_id' => '2',
            'area_id' => '1',
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $user4->assignRole('Staff');
        //Sect. Head
        $user5 = User::create([
            'name' => 'Account Sect. Head',
            'nik' => '3201011308980013',
            'password' => Hash::make('123456'),
            'golongan' => 'kerja',
            'jabatan_id' => '3',
            'area_id' => '1',
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $user5->assignRole('SH');
        //Dept. Head
        $user6 = User::create([
            'name' => 'Account Dept. Head',
            'nik' => '3201011308980014',
            'password' => Hash::make('123456'),
            'golongan' => 'kerja',
            'jabatan_id' => '4',
            'area_id' => '1',
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $user6->assignRole('DH');
        //Div. Head
        $user7 = User::create([
            'name' => 'Account Div. Head',
            'nik' => '3201011308980015',
            'password' => Hash::make('123456'),
            'golongan' => 'kerja',
            'jabatan_id' => '5',
            'area_id' => '1',
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $user7->assignRole('DV');
        //General Manager
        $user8 = User::create([
            'name' => 'Account GM',
            'nik' => '3201011308980016',
            'password' => Hash::make('123456'),
            'golongan' => 'kerja',
            'jabatan_id' => '6',
            'area_id' => '1',
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $user8->assignRole('GM');
        //Direksi
        $user9 = User::create([
            'name' => 'Account Direksi',
            'nik' => '3201011308980017',
            'password' => Hash::make('123456'),
            'golongan' => 'kerja',
            'jabatan_id' => '7',
            'area_id' => '1',
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
        $user9->assignRole('Direksi');
    }
}
