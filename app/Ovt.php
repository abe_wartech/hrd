<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ovt extends Model
{
    protected $table = 'overtime';
    protected $fillable = [
        'user_id', 'jam_mulai', 'jam_akhir', 'jam_kerja', 'status', 'overtime', 'ovt', 'day', 'golongan', 'ovt_type', 'ovt_activity', 'keterangan', 'roundtime',
    ];
}
