<?php

namespace App\Http\Middleware;

use App\Models\BackpackUser as User;
use Closure;

class CheckRoles
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd(backpack_auth()->user());
        # check if user not login redirect to login page
        if (backpack_auth()->guest()) {
            \Alert::warning('Login Terlebih Dahulu')->flash();
            return redirect('/');
        }

        # check function argument to get request from route "roles"
        $roles = array_slice(func_get_args(), 2);
        $user = User::find(backpack_auth()->id());
        if ($user->jabatan_id == '1') {
            $user->assignRole('HRD');
        }
        if ($user->jabatan_id == '2') {
            $user->assignRole('Staff');
        }

        # check user->role == roles from argument
        foreach ($roles as $key => $value) {
            if ($user->hasRole($value)) {
                return $next($request);
            }
        }

        \Alert::warning('Anda Tidak Memiliki Akses')->flash();
        return back();

    }
}
