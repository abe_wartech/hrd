<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\KaryawanRequest as StoreRequest;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\KaryawanRequest as UpdateRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\CrudPanel;

/**
 * Class KaryawanCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class KaryawanCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
         */
        $this->crud->setModel('App\Models\Karyawan');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/karyawan');
        $this->crud->setEntityNameStrings('karyawan', 'karyawan');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
         */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->setColumns([
            [
                'name' => 'id',
                'label' => 'Id',
                'type' => 'text',
            ],
            [
                'name' => 'nik',
                'label' => 'NIK',
                'type' => 'text',
            ],
            [
                'name' => 'pt',
                'label' => 'PT',
                'type' => 'text',
            ],
            [
                'name' => 'nama',
                'label' => 'Nama',
                'type' => 'text',
            ],
            [
                'name' => 'dept',
                'label' => 'Dept',
                'type' => 'text',
            ],
            [
                'name' => 'sect',
                'label' => 'Sect',
                'type' => 'text',
            ],
            [
                'name' => 'status',
                'label' => 'Status',
                'type' => 'text',
            ],
            [
                'name' => 'jabatan',
                'label' => 'Jabatan',
                'type' => 'text',
            ],
            [
                'name' => 'tgl_msuk',
                'label' => 'Tgl Masuk',
                'type' => 'text',
            ],
            [
                'name' => 'prob',
                'label' => 'Prob',
                'type' => 'text',
            ],
            [
                'name' => 'tgl_keluar',
                'label' => 'Tgl Keluar',
                'type' => 'text',
            ],
            [
                'name' => 'no_paklaring',
                'label' => 'No Paklaring',
                'type' => 'text',
            ],
            [
                'name' => 'lokasi',
                'label' => 'Lokasi',
                'type' => 'text',
            ],
            [
                'name' => 'tgl_lahir',
                'label' => 'Tgl Lahir',
                'type' => 'text',
            ],
            [
                'name' => 'tvb',
                'label' => 'TVB',
                'type' => 'text',
            ],
            [
                'name' => 'dept_code',
                'label' => 'Dept Code',
                'type' => 'text',
            ],
            [
                'name' => 'level',
                'label' => 'Level',
                'type' => 'text',
            ],
            [
                'name' => 'nikah',
                'label' => 'Nikah',
                'type' => 'text',
            ],
        ]);

        $this->crud->addFields([
            [
                'name' => 'nik',
                'label' => 'NIK',
                'type' => 'text',
            ],
        ], 'create');
        $this->crud->addFields([
            [
                'name' => 'pt',
                'label' => 'PT',
                'type' => 'text',
            ],
            [
                'name' => 'nama',
                'label' => 'Nama',
                'type' => 'text',
            ],
            [
                'name' => 'dept',
                'label' => 'Dept',
                'type' => 'text',
            ],
            [
                'name' => 'sect',
                'label' => 'Sect',
                'type' => 'text',
            ],
            [
                'name' => 'status',
                'label' => 'Status',
                'type' => 'text',
            ],
            [
                'name' => 'jabatan',
                'label' => 'Jabatan',
                'type' => 'text',
            ],
            [
                'name' => 'tgl_msuk',
                'label' => 'Tgl Masuk',
                'type' => 'date',
            ],
            [
                'name' => 'prob',
                'label' => 'Prob',
                'type' => 'text',
            ],
            [
                'name' => 'tgl_keluar',
                'label' => 'Tgl Keluar',
                'type' => 'text',
            ],
            [
                'name' => 'no_paklaring',
                'label' => 'No Paklaring',
                'type' => 'text',
            ],
            [
                'name' => 'lokasi',
                'label' => 'Lokasi',
                'type' => 'text',
            ],
            [
                'name' => 'tgl_lahir',
                'label' => 'Tgl Lahir',
                'type' => 'text',
            ],
            [
                'name' => 'tvb',
                'label' => 'TVB',
                'type' => 'text',
            ],
            [
                'name' => 'dept_code',
                'label' => 'Dept Code',
                'type' => 'text',
            ],
            [
                'name' => 'level',
                'label' => 'Level',
                'type' => 'text',
            ],
            [
                'name' => 'nikah',
                'label' => 'Nikah',
                'type' => 'text',
            ],
        ]);

        // add asterisk for fields that are required in KaryawanRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
