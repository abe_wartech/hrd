<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\KaryawanBiodataRequest as StoreRequest;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\KaryawanBiodataRequest as UpdateRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\CrudPanel;

/**
 * Class KaryawanBiodataCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class KaryawanBiodataCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
         */
        $this->crud->setModel('App\Models\KaryawanBiodata');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/karyawanbiodata');
        $this->crud->setEntityNameStrings('karyawan biodata', 'karyawan biodata');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
         */

        // TODO: remove setFromDb() and manually define Fields and Columns
        // $this->crud->setFromDb();
        $this->crud->setColumns([
            [
                'name' => 'id',
                'label' => 'Id',
                'type' => 'text',
            ],
            [
                'name' => 'NIK',
                'label' => 'NIK',
                'type' => 'text',
            ],
            [
                'name' => 'NAMA',
                'label' => 'Nama',
                'type' => 'text',
            ],
            [
                'name' => 'GENDER',
                'label' => 'Gender',
                'type' => 'text',
            ],
            [
                'name' => 'TEMPAT_LHR',
                'label' => 'Tempat Lahir',
                'type' => 'text',
            ],
            [
                'name' => 'tgl_lahir',
                'label' => 'Tgl Lahir',
                'type' => 'text',
            ],
            [
                'name' => 'STATUS_PEGAWAI',
                'label' => 'Status Pegawai',
                'type' => 'text',
            ],
            [
                'name' => 'NO_TLP',
                'label' => 'No Telepon',
                'type' => 'text',
            ],
            [
                'name' => 'GOL',
                'label' => 'Golongan',
                'type' => 'text',
            ],
            [
                'name' => 'GRADE',
                'label' => 'Grade',
                'type' => 'text',
            ],
            [
                'name' => 'TANGGAL_MASUK',
                'label' => 'Tgl Masuk',
                'type' => 'text',
            ],
            [
                'name' => 'TANGGAL_KELUAR',
                'label' => 'Tgl Keluar',
                'type' => 'text',
            ],
            [
                'name' => 'JABATAN',
                'label' => 'Jabatan',
                'type' => 'text',
            ],
            [
                'name' => 'AGAMA',
                'label' => 'Agama',
                'type' => 'text',
            ],
            [
                'name' => 'KEBANGSAAN',
                'label' => 'Kebangsaan',
                'type' => 'text',
            ],
            [
                'name' => 'STATUS_NIKAH',
                'label' => 'Status Nikah',
                'type' => 'text',
            ],
            [
                'name' => 'JML_TANGGUNGAN',
                'label' => 'Jumlah Tanggungan',
                'type' => 'text',
            ],
            [
                'name' => 'ALAMAT_TINGGAL',
                'label' => 'Alamat Tinggal',
                'type' => 'text',
            ],
            [
                'name' => 'KOTA',
                'label' => 'Kota',
                'type' => 'text',
            ],
            [
                'name' => 'ALAMAT_KTP',
                'label' => 'Alamat KTP',
                'type' => 'text',
            ],
            [
                'name' => 'EMAIL',
                'label' => 'Email',
                'type' => 'text',
            ],
            [
                'name' => 'NO_KTP',
                'label' => 'No KTP',
                'type' => 'text',
            ],
            [
                'name' => 'PENDIDIKAN',
                'label' => 'Pendidikan',
                'type' => 'text',
            ],
            [
                'name' => 'JURUSAN',
                'label' => 'Jurusan',
                'type' => 'text',
            ],
            [
                'name' => 'KANTOR/PLANT',
                'label' => 'Kantor / Plant',
                'type' => 'text',
            ],
            [
                'name' => 'BANK',
                'label' => 'Bank',
                'type' => 'text',
            ],
            [
                'name' => 'CABANG_BANK',
                'label' => 'Cabang Bank',
                'type' => 'text',
            ],
            [
                'name' => 'NO_REK',
                'label' => 'No Rekening',
                'type' => 'text',
            ],
            [
                'name' => 'NPWP',
                'label' => 'NPWP',
                'type' => 'text',
            ],
            [
                'name' => 'TGL_PENGUKUHAN_NPWP',
                'label' => 'Tgl Pengukuhan NPWP',
                'type' => 'text',
            ],
            [
                'name' => 'NO_JAMSOSTEK',
                'label' => 'No Jamsostek',
                'type' => 'text',
            ],
            [
                'name' => 'TGL_MSK_JAMSOSTEK',
                'label' => 'Tgl Masuk Jamsostek',
                'type' => 'text',
            ],
            [
                'name' => 'ID_SO',
                'label' => 'ID SO',
                'type' => 'text',
            ],
            [
                'name' => 'GOL_DARAH',
                'label' => 'Gol. Darah',
                'type' => 'text',
            ],
            [
                'name' => 'IBU_KANDUNG',
                'label' => 'Ibu Kandung',
                'type' => 'text',
            ],
            [
                'name' => 'MASA_KTP',
                'label' => 'Masa KTP',
                'type' => 'text',
            ],
            [
                'name' => 'AN_NOREK',
                'label' => 'An Norek',
                'type' => 'text',
            ],
            [
                'name' => 'KD_LBR',
                'label' => 'Kode LBR',
                'type' => 'text',
            ],
            [
                'name' => 'KD_ABS',
                'label' => 'Kode ABS',
                'type' => 'text',
            ],
            [
                'name' => 'PHOTO',
                'label' => 'Foto',
                'type' => 'text',
            ],
        ]);

        $this->crud->addFields([
            [
                'name' => 'NIK',
                'label' => 'NIK',
                'type' => 'text',
                'tab' => '1',
            ],
        ], 'create');
        $this->crud->addFields([
            [
                'name' => 'NAMA',
                'label' => 'Nama',
                'type' => 'text',
                'tab' => '1',
            ],
            [
                'name' => 'GENDER',
                'label' => 'Gender',
                'type' => 'text',
                'tab' => '1',
            ],
            [
                'name' => 'TEMPAT_LHR',
                'label' => 'Tempat Lahir',
                'type' => 'text',
                'tab' => '1',
            ],
            [
                'name' => 'tgl_lahir',
                'label' => 'Tgl Lahir',
                'type' => 'date',
                'tab' => '1',
            ],
            [
                'name' => 'STATUS_PEGAWAI',
                'label' => 'Status Pegawai',
                'type' => 'text',
                'tab' => '1',
            ],
            [
                'name' => 'NO_TLP',
                'label' => 'No Telepon',
                'type' => 'text',
                'tab' => '1',
            ],
            [
                'name' => 'GOL',
                'label' => 'Golongan',
                'type' => 'text',
                'tab' => '1',
            ],
            [
                'name' => 'GRADE',
                'label' => 'Grade',
                'type' => 'text',
                'tab' => '1',
            ],
            [
                'name' => 'TANGGAL_MASUK',
                'label' => 'Tgl Masuk',
                'type' => 'date',
                'tab' => '1',
            ],
            [
                'name' => 'TANGGAL_KELUAR',
                'label' => 'Tgl Keluar',
                'type' => 'date',
                'tab' => '1',
            ],
            [
                'name' => 'JABATAN',
                'label' => 'Jabatan',
                'type' => 'text',
                'tab' => '1',
            ],
            [
                'name' => 'AGAMA',
                'label' => 'Agama',
                'type' => 'text',
                'tab' => '1',
            ],
            [
                'name' => 'KEBANGSAAN',
                'label' => 'Kebangsaan',
                'type' => 'text',
                'tab' => '2',
            ],
            [
                'name' => 'STATUS_NIKAH',
                'label' => 'Status Nikah',
                'type' => 'text',
                'tab' => '2',
            ],
            [
                'name' => 'JML_TANGGUNGAN',
                'label' => 'Jumlah Tanggungan',
                'type' => 'text',
                'tab' => '2',
            ],
            [
                'name' => 'ALAMAT_TINGGAL',
                'label' => 'Alamat Tinggal',
                'type' => 'text',
                'tab' => '2',
            ],
            [
                'name' => 'KOTA',
                'label' => 'Kota',
                'type' => 'text',
                'tab' => '2',
            ],
            [
                'name' => 'ALAMAT_KTP',
                'label' => 'Alamat KTP',
                'type' => 'text',
                'tab' => '2',
            ],
            [
                'name' => 'EMAIL',
                'label' => 'Email',
                'type' => 'text',
                'tab' => '2',
            ],
            [
                'name' => 'NO_KTP',
                'label' => 'No KTP',
                'type' => 'text',
                'tab' => '2',
            ],
            [
                'name' => 'PENDIDIKAN',
                'label' => 'Pendidikan',
                'type' => 'text',
                'tab' => '2',
            ],
            [
                'name' => 'JURUSAN',
                'label' => 'Jurusan',
                'type' => 'text',
                'tab' => '2',
            ],
            [
                'name' => 'KANTOR/PLANT',
                'label' => 'Kantor / Plant',
                'type' => 'text',
                'tab' => '2',
            ],
            [
                'name' => 'BANK',
                'label' => 'Bank',
                'type' => 'text',
                'tab' => '2',
            ],
            [
                'name' => 'CABANG_BANK',
                'label' => 'Cabang Bank',
                'type' => 'text',
                'tab' => '2',
            ],
            [
                'name' => 'NO_REK',
                'label' => 'No Rekening',
                'type' => 'text',
                'tab' => '2',
            ],
            [
                'name' => 'NPWP',
                'label' => 'NPWP',
                'type' => 'text',
                'tab' => '2',
            ],
            [
                'name' => 'TGL_PENGUKUHAN_NPWP',
                'label' => 'Tgl Pengukuhan NPWP',
                'type' => 'text',
                'tab' => '3',
            ],
            [
                'name' => 'NO_JAMSOSTEK',
                'label' => 'No Jamsostek',
                'type' => 'text',
                'tab' => '3',
            ],
            [
                'name' => 'TGL_MSK_JAMSOSTEK',
                'label' => 'Tgl Masuk Jamsostek',
                'type' => 'text',
                'tab' => '3',
            ],
            [
                'name' => 'ID_SO',
                'label' => 'ID SO',
                'type' => 'text',
                'tab' => '3',
            ],
            [
                'name' => 'GOL_DARAH',
                'label' => 'Gol. Darah',
                'type' => 'text',
                'tab' => '3',
            ],
            [
                'name' => 'IBU_KANDUNG',
                'label' => 'Ibu Kandung',
                'type' => 'text',
                'tab' => '3',
            ],
            [
                'name' => 'MASA_KTP',
                'label' => 'Masa KTP',
                'type' => 'text',
                'tab' => '3',
            ],
            [
                'name' => 'AN_NOREK',
                'label' => 'An Norek',
                'type' => 'text',
                'tab' => '3',
            ],
            [
                'name' => 'KD_LBR',
                'label' => 'Kode LBR',
                'type' => 'text',
                'tab' => '3',
            ],
            [
                'name' => 'KD_ABS',
                'label' => 'Kode ABS',
                'type' => 'text',
                'tab' => '3',
            ],
            [
                'name' => 'PHOTO',
                'label' => 'Foto',
                'type' => 'text',
                'tab' => '3',
            ],
        ]);

        // add asterisk for fields that are required in KaryawanBiodataRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
