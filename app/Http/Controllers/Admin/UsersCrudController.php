<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UsersRequest as StoreRequest;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\UsersRequest as UpdateRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\CrudPanel;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UsersCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class UsersCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
         */
        $this->crud->setModel('App\Models\BackpackUser');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/users');
        $this->crud->setEntityNameStrings('users', 'users');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
         */

        // TODO: remove setFromDb() and manually define Fields and Columns
        // $this->crud->setFromDb();
        $this->crud->setColumns([
            [
                'name' => 'name',
                'label' => 'Name',
                'type' => 'text',
            ],
            [
                'name' => 'nik',
                'label' => 'NIK',
                'type' => 'text',
            ],
            [
                'name' => 'golongan',
                'label' => 'Golongan',
                'type' => 'text',
            ],
            [
                'label' => 'Jabatan',
                'type' => 'select',
                'name' => 'jabatan_id',
                'entity' => 'jabatanId',
                'attribute' => 'name',
                'model' => 'App\Models\Role',
            ],
            [
                'name' => 'area_id',
                'label' => 'Area',
                'type' => 'select',
                'entity' => 'areaId',
                'attribute' => 'name',
                'model' => 'App\Models\Area',
            ],
        ]);

        $this->crud->addFields([
            [
                'name' => 'name',
                'label' => 'Name',
                'type' => 'text',
            ],
            [
                'name' => 'nik',
                'label' => 'NIK',
                'type' => 'text',
            ],
            [
                'name' => 'password',
                'label' => 'Password',
                'type' => 'password',
            ],
            [
                'name' => 'golongan',
                'label' => 'Golongan',
                'type' => 'select_from_array',
                'allows_null' => false,
                'options' => ['kerja' => 'Hari Kerja', 'libur5' => 'Hari Libur untuk 5 Hari Kerja', 'libur6' => 'Hari Libur untuk 6 Hari Kerja'],
                'default' => 'kerja',
            ],
            [
                'label' => 'Jabatan',
                'type' => 'select',
                'name' => 'jabatan_id',
                'entity' => 'jabatanId',
                'attribute' => 'name',
                'model' => 'App\Models\Role',
            ],
            [
                'name' => 'area_id',
                'label' => 'Area',
                'type' => 'select',
                'entity' => 'areaId',
                'attribute' => 'name',
                'model' => 'App\Models\Area',
            ],
        ]);

        // add asterisk for fields that are required in UsersRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        $this->handlePasswordInput($request);
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // $user1 = BackpackUser::where('nik', $this->crud->entry->nik)->get();
        // if ($this->crud->entry->jabatan_id == '1') {
        //     $user1->assignRole('HRD');
        // }
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        $this->handlePasswordInput($request);
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    /**
     * Handle password input fields.
     *
     * @param FormRequest $request
     */
    protected function handlePasswordInput(FormRequest $request)
    {
        // Encrypt password if specified.
        if ($request->input('password')) {
            $request->request->set('password', bcrypt($request->input('password')));
        } else {
            $request->request->remove('password');
        }
    }
}
