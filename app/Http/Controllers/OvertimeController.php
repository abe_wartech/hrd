<?php

namespace App\Http\Controllers;

use App\Imports\AbsenImport;
use App\Ovt;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class OvertimeController extends Controller
{

    public function absen(Request $request)
    {
        $request->validate([
            'absen' => 'required|mimes:csv,xls,xlsx',
        ]);
        $import = new AbsenImport;
        Excel::import($import, request()->file('absen'));
        $data = $import->data;
        foreach ($data as $key => $value) {
            //cek golongan (default kerja)
            $golongan = 'kerja';
            if ($value[3] == "FOT") {
                $golongan = 'libur6';
            }
            //rubah timeformat dari excel dari / ke -
            $date = str_replace('/', '-', $value[2]);
            //ambil jam akhir dari excel
            $jam_akhir = $value[5];
            //round time jam akhir jika 15 - 45 itu 30 menit
            $hour = date("H", strtotime($jam_akhir));
            $minute = date("i", strtotime($jam_akhir));
            if ($minute < 15) {
                $jam_akhir = date('H:i', strtotime("$hour:00"));
            } elseif ($minute > 15 and $minute < 46) {
                $jam_akhir = date('H:i', strtotime("$hour:30"));
            } elseif ($minute > 45) {
                $hour = $hour + 1;
                $jam_akhir = date('H:i', strtotime("$hour:00"));
            }
            $ovtsave = new Ovt;
            $ovtsave->user_id = $value[0];
            $ovtsave->jam_mulai = $value[4];
            $ovtsave->jam_akhir = $value[5];
            $ovtsave->jam_kerja = $value[3];
            $ovtsave->status = $value[6];
            $ovtsave->overtime = "";
            $ovtsave->ovt = "";
            $ovtsave->day = new Carbon($date);
            $ovtsave->golongan = $golongan;
            $ovtsave->roundtime = $jam_akhir;
            $ovtsave->start = $value[4];
            $ovtsave->finish = $value[5];
            $ovtsave->save();
        }
        \Alert::success(trans('Data Absen Berhasil Di import!'))->flash();
        return back();
    }
}
