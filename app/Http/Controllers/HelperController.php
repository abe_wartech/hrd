<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\Insentif;
use App\Models\Karyawan;
use App\Models\KaryawanBiodata;
use App\Models\Shift;
use App\Models\Status;
use App\Ovt;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class HelperController extends Controller
{
    public function GetRole()
    {
        $user = backpack_user();
        $username = $user->name;
        $role = $user->jabatan_id;
        switch ($role) {
            case "1":
                $role = "HRD";
                break;
            case "2":
                $role = "Staff";
                break;
            case "3":
                $role = "SH";
                break;
            case "3":
                $role = "DH";
                break;
            case "3":
                $role = "DV";
                break;
            case "3":
                $role = "GM";
                break;
            case "3":
                $role = "Direksi";
                break;
            default:
                $role = "Staff";
                break;
        }

        return response()->json(['role' => $role, 'name' => $username]);
    }

    public function GetWilayah(Request $search)
    {
        $term_search = $search->input("q");
        $area = Area::select('id', 'name')->where('name', 'LIKE', '%' . $term_search . '%')->get();
        return response()->json($area);
    }

    public function GetStatus(Request $search)
    {
        $term_search = $search->input("q");
        $status = Status::select('id', 'name')->where('name', 'LIKE', '%' . $term_search . '%')->get();
        return response()->json($status);
    }

    public function GetDataKaryawan(Request $search)
    {
        $term_search = $search->input("q");
        $karyawan = Karyawan::select('nik', 'nama')->where('nama', 'LIKE', '%' . $term_search . '%')->get();
        return response()->json($karyawan);
    }

    public function GetDataShift(Request $search)
    {
        $term_search = $search->input("q");
        $shift = Shift::select('id', 'shift_id')->where('shift_id', 'LIKE', '%' . $term_search . '%')->get();
        return response()->json($shift);
    }

    public function GetOneDataKaryawan($id)
    {
        $karyawan = Karyawan::where('nik', 'LIKE', '%' . $id . '%')->get();
        return response()->json($karyawan);
    }

    public function GetOneDataShift($id)
    {
        $shift = Shift::where('id', 'LIKE', '%' . $id . '%')->get();
        return response()->json($shift);
    }

    public function GetDataForOvertime(Request $request)
    {
        $startmonth = new Carbon($request->input("startmonth"));
        $endmonth = new Carbon($request->input("endmonth"));
        $startmonthfordisplay = $startmonth->format('d-F');
        $endmonthfordisplay = $endmonth->format('d-F');
        $tahun = $startmonth->format('F');

        $data = DB::table('overtime')
            ->join('karyawan', 'overtime.user_id', '=', 'karyawan.nik')
            ->whereBetween('overtime.day', array($startmonth, $endmonth->addDays(1)))
            ->whereNull('overtime.rapel')
            ->select('overtime.id', 'karyawan.nik', 'karyawan.nama', 'karyawan.dept', 'karyawan.sect', 'overtime.jam_mulai', 'overtime.jam_akhir', 'overtime.day', 'overtime.jam_kerja', 'overtime.overtime', 'overtime.status', 'overtime.created_at')
            ->get();

        if (count($data) != 0) {
            $tahun = $startmonth->format('Y');

            return response()->json([
                "header" => [
                    "jenis" => "Data Overtime",
                    "tahun" => $tahun,
                    "startmonth" => $startmonthfordisplay,
                    "endmonth" => $endmonthfordisplay,
                ], "data" => $data,
            ]);
        }
    }

    public function GetDataForOvertimeRapel(Request $request)
    {
        $startmonth = new Carbon($request->input("startmonth"));
        $endmonth = new Carbon($request->input("endmonth"));
        $startmonthfordisplay = $startmonth->format('d-F');
        $endmonthfordisplay = $endmonth->format('d-F');
        $tahun = $startmonth->format('F');

        $data = DB::table('overtime')
            ->join('karyawan', 'overtime.user_id', '=', 'karyawan.nik')
            ->whereBetween('overtime.rapel', array($startmonth, $endmonth->addDays(1)))
            ->whereNotNull('overtime.rapel')
            ->select('overtime.id', 'karyawan.nik', 'karyawan.nama', 'karyawan.dept', 'karyawan.sect', 'overtime.jam_mulai', 'overtime.jam_akhir', 'overtime.day', 'overtime.jam_kerja', 'overtime.overtime', 'overtime.status', 'overtime.created_at')
            ->get();

        if (count($data) != 0) {
            $tahun = $startmonth->format('Y');

            return response()->json([
                "header" => [
                    "jenis" => "Data Overtime Rapel",
                    "tahun" => $tahun,
                    "startmonth" => $startmonthfordisplay,
                    "endmonth" => $endmonthfordisplay,
                ], "data" => $data,
            ]);
        }
    }

    public function GetOneDataForOvertime($id)
    {
        $ovt = DB::table('overtime')
            ->join('karyawan', 'overtime.user_id', '=', 'karyawan.nik')
            ->join('shift', 'overtime.jam_kerja', '=', 'shift.shift_id')
            ->where('overtime.id', $id)
            ->select('overtime.*', 'karyawan.nik', 'karyawan.nama', 'karyawan.dept', 'karyawan.sect', 'shift.star_ot2')
            ->first();
        return response()->json($ovt);
    }

    public function UpdateOvertime(Request $request, $id)
    {
        $istirahat = $request->istirahat;
        $stot = $request->stot;
        $fnot = $request->fnot;
        $rapel = $request->rapel;
        $ovtsave = Ovt::where('id', $id)->first();
        $insentif = KaryawanBiodata::where('NIK', $ovtsave->user_id)->select('KD_LBR')->first();
        $shiftmulai = Shift::where('shift_id', $request->shift)->select('star_ot2')->first();
        $shiftok = explode(':', $shiftmulai->star_ot2);
        $shiftfix = $shiftok[0] . ':' . $shiftok[1];
        $golongan = $request->gol;
        $date = Carbon::parse($ovtsave->day)->format('Y-m-d');
        $jam_mulai = $request->jam_mulai;
        if ($stot) {
            $jam_mulai = $stot;
        }
        $ovtsave->start = $jam_mulai;
        $hour = date("H", strtotime($jam_mulai));
        $minute = date("i", strtotime($jam_mulai));
        if ($minute < 15) {
            $jam_mulai = date('H:i', strtotime("$hour:00"));
        } elseif ($minute > 15 and $minute < 46) {
            $jam_mulai = date('H:i', strtotime("$hour:30"));
        } elseif ($minute > 45) {
            $hour = $hour + 1;
            $jam_mulai = date('H:i', strtotime("$hour:00"));
        }
        $combinedDTm = date('Y-m-d H:i', strtotime("$date $shiftfix"));
        if (strtok($jam_mulai, ":") > strtok($shiftfix, ":")) {
            $combinedDTm = date('Y-m-d H:i', strtotime("$date $jam_mulai"));
        }
        $jam_akhir = $request->jam_akhir;
        if ($fnot) {
            $jam_akhir = $fnot;
        }
        $ovtsave->finish = $jam_akhir;
        $houra = date("H", strtotime($jam_akhir));
        $minutea = date("i", strtotime($jam_akhir));
        if ($minutea < 15) {
            $jam_akhir = date('H:i', strtotime("$houra:00"));
        } elseif ($minutea > 15 and $minutea < 46) {
            $jam_akhir = date('H:i', strtotime("$houra:30"));
        } elseif ($minutea > 45) {
            $houra = $houra + 1;
            $jam_akhir = date('H:i', strtotime("$houra:00"));
        }
        if (strtok($jam_akhir, ":") >= "00" && strtok($jam_akhir, ":") <= "06" || strtok($jam_akhir, ":") >= strtok($shiftfix, ":")) {
            if (strtok($jam_akhir, ":") >= "00" && strtok($jam_akhir, ":") <= "06") {
                $date = Carbon::parse($date)->addDay()->format('Y-m-d');
            }
            $combinedDTa = date('Y-m-d H:i', strtotime("$date $jam_akhir"));
            $a = Carbon::parse($combinedDTm);
            $b = Carbon::parse($combinedDTa);
            $jam = $a->diffInHours($b);
            if ($request->shift == "S1") {
                $jam = $a->diffInMinutes($b);
            }
            $overtime = $jam;
            if ($request->shift == "S1") {
                $overtime = number_format((float) $jam / 60, 1, '.', '');
            }
            // $ovt = 0;
            // if ($golongan == 'kerja') {
            //     $ovt = ($jam * 2) - 0.5;
            //     if ($request->shift == "S1") {
            //         $ovt = (($jam / 60) * 2) - 0.5;
            //     }
            // } else if ($golongan == 'libur5') {
            //     for ($i = 0; $i < $jam; $i++) {
            //         if ($i < 8) {
            //             $ovt += 2;
            //         }
            //         if ($i == 8) {
            //             $ovt += 3;
            //         }
            //         if ($i > 8) {
            //             $ovt += 4;
            //         }
            //     }
            // } else if ($golongan == 'libur6') {
            //     for ($i = 0; $i < $jam; $i++) {
            //         if ($i < 7) {
            //             $ovt += 2;
            //         }
            //         if ($i == 7) {
            //             $ovt += 3;
            //         }
            //         if ($i > 7) {
            //             $ovt += 4;
            //         }
            //     }
            // }
        } else {
            $overtime = 0;
            // $ovt = 0;
        }
        $overtimefix = $overtime - max(0, $istirahat);
        $ins = 0;
        if ($insentif->KD_LBR == "I6") {
            $kali = Insentif::select('jumlah')->first();
            $ins = $overtimefix * $kali->jumlah;
        }
        $ovtsave->jam_mulai = $request->jam_mulai;
        $ovtsave->jam_akhir = $request->jam_akhir;
        $ovtsave->jam_kerja = $request->shift;
        $ovtsave->status = $request->status;
        $ovtsave->overtime = $overtimefix;
        // $ovtsave->ovt = max(0, $ovt);
        // $ovtsave->day = new Carbon($request->tanggal);
        $ovtsave->golongan = $golongan;
        $ovtsave->ovt_type = $request->ovt_type;
        $ovtsave->ovt_activity = $request->ovt_activity;
        $ovtsave->keterangan = $request->ket;
        $ovtsave->stot = $stot;
        $ovtsave->fnot = $fnot;
        $ovtsave->roundtime = $jam_akhir;
        $ovtsave->istirahat = $istirahat;
        if ($request->rapel) {
            $ovtsave->rapel = new Carbon($rapel);
        }
        $ovtsave->insentif = number_format($ins);
        $ovtsave->save();

        return response()->json($ovtsave);
    }

    public function CalcOvertime($id)
    {
        $ovtsave = Ovt::where('id', $id)->first();
        $insentif = KaryawanBiodata::where('NIK', $ovtsave->user_id)->select('KD_LBR')->first();
        $shiftmulai = Shift::where('shift_id', $ovtsave->jam_kerja)->select('star_ot2')->first();
        $shiftok = explode(':', $shiftmulai->star_ot2);
        $shiftfix = $shiftok[0] . ':' . $shiftok[1];
        $golongan = $ovtsave->golongan;
        $date = Carbon::parse($ovtsave->day)->format('Y-m-d');
        $jam_mulai = $ovtsave->jam_mulai;
        if ($ovtsave->stot) {
            $jam_mulai = $stot;
        }
        $ovtsave->start = $jam_mulai;
        $hourm = date("H", strtotime($jam_mulai));
        $minutem = date("i", strtotime($jam_mulai));
        if ($minutem < 15) {
            $jam_mulai = date('H:i', strtotime("$hourm:00"));
        } elseif ($minutem > 15 and $minutem < 46) {
            $jam_mulai = date('H:i', strtotime("$hourm:30"));
        } elseif ($minutem > 45) {
            $hourm = $hourm + 1;
            $jam_mulai = date('H:i', strtotime("$hourm:00"));
        }
        $combinedDTm = date('Y-m-d H:i', strtotime("$date $shiftfix"));
        if (strtok($jam_mulai, ":") > strtok($shiftfix, ":")) {
            $combinedDTm = date('Y-m-d H:i', strtotime("$date $jam_mulai"));
        }
        $jam_akhir = $ovtsave->jam_akhir;
        if ($ovtsave->fnot) {
            $jam_akhir = $fnot;
        }
        $ovtsave->finish = $jam_akhir;
        $hour = date("H", strtotime($jam_akhir));
        $minute = date("i", strtotime($jam_akhir));
        if ($minute < 15) {
            $jam_akhir = date('H:i', strtotime("$hour:00"));
        } elseif ($minute > 15 and $minute < 46) {
            $jam_akhir = date('H:i', strtotime("$hour:30"));
        } elseif ($minute > 45) {
            $hour = $hour + 1;
            $jam_akhir = date('H:i', strtotime("$hour:00"));
        }
        if (strtok($jam_akhir, ":") >= "00" && strtok($jam_akhir, ":") <= "06" || strtok($jam_akhir, ":") >= strtok($shiftfix, ":")) {
            if (strtok($jam_akhir, ":") >= "00" && strtok($jam_akhir, ":") <= "06") {
                $date = Carbon::parse($date)->addDay()->format('Y-m-d');
            }
            $combinedDTa = date('Y-m-d H:i', strtotime("$date $jam_akhir"));
            $a = Carbon::parse($combinedDTm);
            $b = Carbon::parse($combinedDTa);
            $jam = $a->diffInHours($b);
            if ($ovtsave->jam_kerja == "S1") {
                $jam = $a->diffInMinutes($b);
            }
            $overtime = $jam;
            if ($ovtsave->jam_kerja == "S1") {
                $overtime = number_format((float) $jam / 60, 1, '.', '');
            }
            // $ovt = 0;
            // if ($golongan == 'kerja') {
            //     $ovt = ($jam * 2) - 0.5;
            //     if ($ovtsave->jam_kerja == "S1") {
            //         $ovt = (($jam / 60) * 2) - 0.5;
            //     }
            // } else if ($golongan == 'libur5') {
            //     for ($i = 0; $i < $jam; $i++) {
            //         if ($i < 8) {
            //             $ovt += 2;
            //         }
            //         if ($i == 8) {
            //             $ovt += 3;
            //         }
            //         if ($i > 8) {
            //             $ovt += 4;
            //         }
            //     }
            // } else if ($golongan == 'libur6') {
            //     for ($i = 0; $i < $jam; $i++) {
            //         if ($i < 7) {
            //             $ovt += 2;
            //         }
            //         if ($i == 7) {
            //             $ovt += 3;
            //         }
            //         if ($i > 7) {
            //             $ovt += 4;
            //         }
            //     }
            // }
        } else {
            $overtime = 0;
            // $ovt = 0;
        }
        $ins = 0;
        if ($insentif->KD_LBR == "I6") {
            $kali = Insentif::select('jumlah')->first();
            $ins = $overtime * $kali->jumlah;
        }
        $ovtsave->overtime = $overtime;
        // $ovtsave->ovt = max(0, $ovt);
        $ovtsave->insentif = number_format($ins);
        $ovtsave->save();

        return response()->json($ovtsave);
    }
}
