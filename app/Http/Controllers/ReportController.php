<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function ReportTable()
    {
        return view('report.reporttable');
    }

    public function ReportBar()
    {
        return view('report.reportbar');
    }

    public function ReportTableRapel()
    {
        return view('report.reporttablerapel');
    }

    public function ReportBarRapel()
    {
        return view('report.reportbarrapel');
    }

    public function GetDataForTable(Request $request)
    {
        $startmonth = new Carbon($request->input("startmonth"));
        $endmonth = new Carbon($request->input("endmonth"));
        $startmonthfordisplay = $startmonth->format('d-F');
        $endmonthfordisplay = $endmonth->format('d-F');
        $tahun = $startmonth->format('F');

        $data = DB::table('overtime')
            ->join('karyawan', 'overtime.user_id', '=', 'karyawan.nik')
            ->whereIn('karyawan.pt', explode(",", $request->input("pt")))
            ->whereBetween('overtime.day', array($startmonth, $endmonth->addDays(1)))
            ->whereNull('overtime.rapel')
            ->select('overtime.id', 'overtime.day', 'overtime.golongan', 'karyawan.nik', 'karyawan.nama', 'overtime.start', 'overtime.finish', 'overtime.istirahat', 'overtime.created_at')
            ->get();

        if (count($data) != 0) {
            $tahun = $startmonth->format('Y');

            return response()->json([
                "header" => [
                    "tahun" => $tahun,
                    "startmonth" => $startmonthfordisplay,
                    "endmonth" => $endmonthfordisplay,
                ], "data" => $data,
            ]);
        }
    }

    public function GetDataForTableRapel(Request $request)
    {
        $startmonth = new Carbon($request->input("startmonth"));
        $endmonth = new Carbon($request->input("endmonth"));
        $startmonthfordisplay = $startmonth->format('d-F');
        $endmonthfordisplay = $endmonth->format('d-F');
        $tahun = $startmonth->format('F');

        $data = DB::table('overtime')
            ->join('karyawan', 'overtime.user_id', '=', 'karyawan.nik')
            ->whereIn('karyawan.pt', explode(",", $request->input("pt")))
            ->whereBetween('overtime.rapel', array($startmonth, $endmonth->addDays(1)))
            ->whereNotNull('overtime.rapel')
            ->select('overtime.id', 'overtime.day', 'overtime.golongan', 'karyawan.nik', 'karyawan.nama', 'overtime.start', 'overtime.finish', 'overtime.istirahat', 'overtime.created_at')
            ->get();

        if (count($data) != 0) {
            $tahun = $startmonth->format('Y');

            return response()->json([
                "header" => [
                    "tahun" => $tahun,
                    "startmonth" => $startmonthfordisplay,
                    "endmonth" => $endmonthfordisplay,
                ], "data" => $data,
            ]);
        }
    }

    public function GetDataForChart(Request $request)
    {
        $startmonth = new Carbon($request->input("startmonth"));
        $endmonth = new Carbon($request->input("endmonth"));
        $startmonthfordisplay = $startmonth->format('d-F');
        $endmonthfordisplay = $endmonth->format('d-F');
        $tahun = $startmonth->format('F');

        $data = DB::table('overtime')
            ->join('karyawan', 'overtime.user_id', '=', 'karyawan.nik')
            ->whereIn('karyawan.pt', explode(",", $request->input("pt")))
            ->whereBetween('overtime.day', array($startmonth, $endmonth->addDays(1)))
            ->whereNull('overtime.rapel')
            ->select('overtime.id', 'karyawan.nik', 'karyawan.nama', 'karyawan.dept', 'karyawan.sect', 'overtime.jam_mulai', 'overtime.jam_akhir', 'overtime.day', 'overtime.jam_kerja', 'overtime.overtime', 'overtime.status', 'overtime.created_at')
            ->get();

        if (count($data) != 0) {
            $tahun = $startmonth->format('Y');

            return response()->json([
                "header" => [
                    "tahun" => $tahun,
                    "startmonth" => $startmonthfordisplay,
                    "endmonth" => $endmonthfordisplay,
                ], "data" => $data,
            ]);
        }
    }

    public function GetDataForChartRapel(Request $request)
    {
        $startmonth = new Carbon($request->input("startmonth"));
        $endmonth = new Carbon($request->input("endmonth"));
        $startmonthfordisplay = $startmonth->format('d-F');
        $endmonthfordisplay = $endmonth->format('d-F');
        $tahun = $startmonth->format('F');

        $data = DB::table('overtime')
            ->join('karyawan', 'overtime.user_id', '=', 'karyawan.nik')
            ->whereIn('karyawan.pt', explode(",", $request->input("pt")))
            ->whereBetween('overtime.rapel', array($startmonth, $endmonth->addDays(1)))
            ->whereNotNull('overtime.rapel')
            ->select('overtime.id', 'karyawan.nik', 'karyawan.nama', 'karyawan.dept', 'karyawan.sect', 'overtime.jam_mulai', 'overtime.jam_akhir', 'overtime.day', 'overtime.jam_kerja', 'overtime.overtime', 'overtime.status', 'overtime.created_at')
            ->get();

        if (count($data) != 0) {
            $tahun = $startmonth->format('Y');

            return response()->json([
                "header" => [
                    "tahun" => $tahun,
                    "startmonth" => $startmonthfordisplay,
                    "endmonth" => $endmonthfordisplay,
                ], "data" => $data,
            ]);
        }
    }
}
