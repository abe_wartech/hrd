<?php

namespace App\Models;

use App\User;
use Backpack\Base\app\Models\Traits\InheritsRelationsFromParentModel;
use Backpack\Base\app\Notifications\ResetPasswordNotification as ResetPasswordNotification;
use Backpack\CRUD\CrudTrait;
use Spatie\Permission\Traits\HasRoles;

class BackpackUser extends User
{
    use CrudTrait; // <----- this
    use HasRoles; // <------ and this
    use InheritsRelationsFromParentModel;

    protected $guard_name = 'api';

    protected $table = 'users';

    protected $fillable = ['name', 'nik', 'password', 'golongan', 'jabatan_id', 'area_id'];

    /**
     * Send the password reset notification.
     *
     * @param string $token
     *
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * Get the e-mail address where password reset links are sent.
     *
     * @return string
     */
    public function getEmailForPasswordReset()
    {
        return $this->email;
    }

    public function areaId()
    {
        return $this->hasOne('App\Models\Area', 'id', 'area_id');
    }

    public function jabatanId()
    {
        return $this->hasOne('App\Models\Role', 'id', 'jabatan_id');
    }

}
