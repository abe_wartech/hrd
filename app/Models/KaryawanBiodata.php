<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class KaryawanBiodata extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
     */

    protected $table = 'karyawan_biodata';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['NIK', 'NAMA', 'GENDER', 'TEMPAT_LHR', 'tgl_lahir', 'STATUS_PEGAWAI', 'NO_TLP', 'GOL', 'GRADE', 'TANGGAL_MASUK', 'TANGGAL_KELUAR', 'JABATAN', 'AGAMA', 'KEBANGSAAN', 'STATUS_NIKAH', 'JML_TANGGUNGAN', 'ALAMAT_TINGGAL', 'KOTA', 'ALAMAT_KTP', 'EMAIL', 'NO_KTP', 'PENDIDIKAN', 'JURUSAN', 'KANTOR/PLANT', 'BANK', 'CABANG_BANK', 'NO_REK', 'NPWP', 'TGL_PENGUKUHAN_NPWP', 'NO_JAMSOSTEK', 'TGL_MSK_JAMSOSTEK', 'ID_SO', 'GOL_DARAH', 'IBU_KANDUNG', 'MASA_KTP', 'AN_NOREK', 'KD_LBR', 'KD_ABS', 'PHOTO'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
     */

    /*
|--------------------------------------------------------------------------
| MUTATORS
|--------------------------------------------------------------------------
 */
}
